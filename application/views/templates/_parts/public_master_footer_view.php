<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $current_lang = $_SESSION['set_language'];
?>
        <footer>
            <div class="container">
                <div class="footerIn">
                    <ul>
                        <a href="http://www.indianpac.com" target="_blank"><li>I-PAC</li></a>
                        <a href="<?php echo base_url();?>assets/pdf/YIP_Concept_Note.pdf" target="_blank"><li><?php echo $this->lang->line('concept_note'); ?></li></a>
                        <a href="<?php echo $this->lang->line('link_faq'); ?>" target="_blank"><li><?php echo $this->lang->line('faq'); ?></li></a>
                        <a href="<?php echo base_url();?>assets/pdf/YIP_Privacy_Policy.pdf" target="_blank"><li class="openPopup" id="privacy"><?php echo $this->lang->line('privacy_policy'); ?></li></a>
                        <a href="<?php echo base_url();?>assets/pdf/YIP_Terms_Condition.pdf" target="_blank"><li class="openPopup" id="terms"><?php echo $this->lang->line('terms'); ?></li></a>
                    </ul>
                    <div class="copyright">
                        <div class="copyright-ipac">Indian PAC All rights reserved ©2018 &nbsp;|&nbsp;</div>
                        <div class="follow_flex">
                            <span style="float:left;">Follow us:&nbsp;</span>
                            <a href="https://www.facebook.com/IndianPAC/" target="_blank" style="float:left;line-height: 0px">
                                <img height="30px" src="https://www.indianpac.com/naf/assets/images/fb.svg">
                            </a>
                            <a href="https://twitter.com/IndianPAC" target="_blank" style="float:left;line-height: 0px">
                                <img height="30px" src="https://www.indianpac.com/naf/assets/images/twit.svg">
                            </a>
<!--                            <a href="https://www.instagram.com/indianPAC/" target="_blank" style="float:left;line-height: 0px">
                                <img height="30px" src="https://www.indianpac.com/naf/assets/images/insta.png">
                                -->
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <div class="modal" id="myModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Gallery Items</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="https://images.pexels.com/photos/853168/pexels-photo-853168.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-target="#image-gallery">
                            <img class="img-thumbnail" src="https://images.pexels.com/photos/853168/pexels-photo-853168.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Another alt text">
                        </a>
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="https://images.pexels.com/photos/158971/pexels-photo-158971.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-target="#image-gallery">
                            <img class="img-thumbnail" src="https://images.pexels.com/photos/158971/pexels-photo-158971.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Another alt text">
                        </a>
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="Im so nice" data-image="https://images.pexels.com/photos/305070/pexels-photo-305070.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" data-target="#image-gallery">
                            <img class="img-thumbnail" src="https://images.pexels.com/photos/305070/pexels-photo-305070.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" alt="Another alt text">
                        </a>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!--Modal: Name-->
        <div class="modal fade" id="modalYT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <!--Content-->
                <div class="modal-content">
                    <!--Body-->
                    <div class="modal-body mb-0 p-0">
                        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/6y3ZVWuITu0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>
        <!--Modal: Name-->
        
        <!--End of Tawk.to Script-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery-ui.min.js"></script>
        <script src="https://connect.facebook.net/en_US/all.js"></script>
        <script src="https://res.cloudinary.com/indianpac/raw/upload/naf/minified_js/jquery.waypoints.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.0/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/javascript.util/0.12.12/javascript.util.min.js"></script>
        
        <script type="text/javascript">
            let modalId = $('#image-gallery');
            $(document).ready(function() {
                loadGallery(true, 'a.thumbnail');
                //This function disables buttons when needed
                function disableButtons(counter_max, counter_current) {
                    $('#show-previous-image, #show-next-image')
                        .show();
                    if (counter_max === counter_current) {
                        $('#show-next-image')
                            .hide();
                    } else if (counter_current === 1) {
                        $('#show-previous-image')
                            .hide();
                    }
                }
                
                function loadGallery(setIDs, setClickAttr) {
                    let current_image,
                        selector,
                        counter = 0;

                    $('#show-next-image, #show-previous-image')
                        .click(function() {
                            if ($(this)
                                .attr('id') === 'show-previous-image') {
                                current_image--;
                            } else {
                                current_image++;
                            }

                            selector = $('[data-image-id="' + current_image + '"]');
                            updateGallery(selector);
                        });

                    function updateGallery(selector) {
                        let $sel = selector;
                        current_image = $sel.data('image-id');
                        $('#image-gallery-title')
                            .text($sel.data('title'));
                        $('#image-gallery-image')
                            .attr('src', $sel.data('image'));
                        disableButtons(counter, $sel.data('image-id'));
                    }

                    if (setIDs == true) {
                        $('[data-image-id]')
                            .each(function() {
                                counter++;
                                $(this)
                                    .attr('data-image-id', counter);
                            });
                    }
                    $(setClickAttr)
                        .on('click', function() {
                            updateGallery($(this));
                        });
                }
            });

            // build key actions
            $(document).keydown(function(e) {
                switch (e.which) {
                    case 37: // left
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
                            $('#show-previous-image')
                                .click();
                        }
                        break;

                    case 39: // right
                        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
                            $('#show-next-image')
                                .click();
                        }
                        break;

                    default:
                        return; // exit this handler for other keys
                }
                e.preventDefault(); // prevent the default action (scroll / move caret)
            });
            
            $(document).ready(function() {
                $('.video-image-overlay img').click(function() {
                    var video = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="' + $('.video-image-overlay img').attr('data-video') + '"></iframe></div>';
                    $('.about_ipac_video').html(video);
                    // $('.close_video').show();
                    $(this).hide();
                });
                /* $('.close_video').click(function() {
                    $('.video-image-overlay img').show();
                    $('.about_ipac_video').empty();
                    $(this).hide();
                }); */
            });

            $('.digital_numbers').each(function () {
                $(this).prop('Counter',0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        </script>
    </body>
</html>