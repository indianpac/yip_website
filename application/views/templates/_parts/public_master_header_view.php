<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    header('X-Frame-Options: SAMEORIGIN');

    $current_lang = $_SESSION['set_language'];
?>
<!DOCTYPE html>
<html lang="<?php echo $current_lang;?>">
    <head>
        <title><?php echo $page_title;?></title>
        <meta name="description" content="" />
        <meta http-equiv="Cache-control" content="no-cache" />
        <meta charset="utf-8" />
        <meta name="MobileOptimized" content="320" />
        <?php
            foreach($langs as $lang_slug=>$lang) {
                if($lang_slug!=$current_lang) {
                    echo '<link rel="alternate" href="'.site_url($lang['alternate_link']).'" hreflang="'.str_replace('_','-',$lang['language_code']).'" />'."\r\n";
                }
            }
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"/>
        <meta name="google-signin-client_id" content="784213681989-6q1ei1mlr5reo5rdkd2u631tnc26nj2a.apps.googleusercontent.com">
        <meta name="google-signin-scope" content="https://www.googleapis.com/auth/analytics.readonly">
        <link rel="icon" href="<?php echo base_url()?>assets/images/logo.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/yip_surya.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/jquery-responsiveGallery.css">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121633598-5"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-121633598-5');
        </script>
    </head>

    <body class="home home-wrapper">
        <!--header code-->
        <div class="container-fluid text-center banner_bg">
            <div class="container">
                <div class="row justify-content-start">
                    <div class="col-12">
                        <div class="header">
                            <img src="<?php echo base_url()?>assets/images/logo.png"/>
                            <ul>
                                <?php
                                    foreach($langs as $slug => $language) {
                                        echo '<li class="'.$language['language_directory'].'">' . anchor($language['alternate_link'], $language['name']) . '</li>';
                                    }
                                ?>
                            </ul>
                        </div>
                        <div class="content_header_banner mobile">
                            <div class="button_register_div">
                                <a href="<?php echo base_url();?>home/influencers" style="text-decoration:none !important; "><button class="button_register"><?php echo $this->lang->line('register_now'); ?></button></a>
                                <div class="see_how_it_works" data-toggle="modal" data-target="#modalYT"><img src="<?php echo base_url() ?>assets/images/play.png"> <?php echo $this->lang->line('see_how_it_works'); ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 content_header_banner desktop">
                        <h1><b><?php echo $this->lang->line('enter_politics'); ?></b></h1>
                        <p><?php echo $this->lang->line('enter_politics_description'); ?></p>
                        <div class="button_register_div">
                            <a href="<?php echo base_url();?>home/influencers" style="text-decoration:none !important; "><button class="button_register"><?php echo $this->lang->line('register_now'); ?></button></a>
                            <div class="see_how_it_works" data-toggle="modal" data-target="#modalYT"><img src="<?php echo base_url() ?>assets/images/play.png"> <?php echo $this->lang->line('see_how_it_works'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mobile Text -->
        <div class="container-fluid text-center mobile_text mobile">
            <div class="container">
                <div class="row justify-content-start">
                    <div class="col-12 content_header_banner">
                        <h1><b><?php echo $this->lang->line('enter_politics'); ?></b></h1>
                        <p><?php echo $this->lang->line('enter_politics_description'); ?></p>
                    </div>
                </div>
            </div>
        </div>