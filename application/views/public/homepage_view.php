<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $current_lang = $_SESSION['set_language'];
?>
        <!-- Numbers STARTS-->
        <div class="container-fluid campaign-numbers bg_banner_india">
            <div class="container">
                <div class="row justify-content-start">
                    <div class="col-6 col-md-3 col-sm-6 col-xs-6 event_numbers">
                        <img src="<?php echo base_url();?>assets/images/icon-1.png"/>
                        <div class="event_numbers_content">
                            <p><?php echo $this->lang->line('total_registerations'); ?></p>
                            <p class='digital_numbers'>48213</p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-sm-6 col-xs-6 event_numbers">
                        <img src="<?php echo base_url();?>assets/images/icon-2.png"/>
                        <div class="event_numbers_content">
                            <p><?php echo $this->lang->line('tot_events'); ?></p>
                            <p class='digital_numbers'>474</p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-sm-6 col-xs-6 event_numbers">
                        <img src="<?php echo base_url();?>assets/images/icon-3.png"/>
                        <div class="event_numbers_content">
                            <p><?php echo $this->lang->line('tot_dis'); ?></p>
                            <p class='digital_numbers'>514</p>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-sm-6 col-xs-6 event_numbers">
                        <img src="<?php echo base_url();?>assets/images/icon-4.png"/>
                        <div class="event_numbers_content">
                            <p><?php echo $this->lang->line('tot_att'); ?></p>
                            <p class='digital_numbers'>74216</p>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="first_slider">
                            <h2><?php echo $this->lang->line('fact_heading'); ?></h2>
                            <?php
                                if($current_lang == 'en') {
                            ?>
                                    <div id="carouselExampleControls" class="carousel slide desktop">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_1.png" alt="First slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_2.png" alt="Second slide">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                            <img src="<?php echo base_url();?>assets/images/left_arrow.png" alt="Prev">
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                            <img src="<?php echo base_url();?>assets/images/right_arrow.png" alt="Next">
                                        </a>
                                    </div>

                                    <div id="carouselExampleControlsMobile" class="carousel slide mobile">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_mobile_1.png" alt="First slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_mobile_2.png" alt="Second slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_mobile_3.png" alt="Second slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_mobile_4.png" alt="Second slide">
                                            </div>
                                        </div>
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleControlsMobile" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleControlsMobile" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleControlsMobile" data-slide-to="2"></li>
                                            <li data-target="#carouselExampleControlsMobile" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                            <?php
                                } else {
                            ?>
                                    <div id="carouselExampleControlsHindi" class="carousel slide desktop">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_hin_1.png" alt="First slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_hin_2.png" alt="Second slide">
                                            </div>
                                        </div>
                                        <a class="carousel-control-prev" href="#carouselExampleControlsHindi" role="button" data-slide="prev">
                                            <img src="<?php echo base_url();?>assets/images/left_arrow.png" alt="Prev">
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleControlsHindi" role="button" data-slide="next">
                                            <img src="<?php echo base_url();?>assets/images/right_arrow.png" alt="Next">
                                        </a>
                                    </div>

                                    <div id="carouselExampleControlsHindiMobile" class="carousel slide mobile">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_hin_mobile_1.png" alt="First slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_hin_mobile_2.png" alt="Second slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_hin_mobile_3.png" alt="Second slide">
                                            </div>
                                            <div class="carousel-item">
                                                <img class="d-block w-100" src="<?php echo base_url();?>assets/images/main_slider_hin_mobile_4.png" alt="Second slide">
                                            </div>
                                        </div>
                                        <ol class="carousel-indicators">
                                            <li data-target="#carouselExampleControlsHindiMobile" data-slide-to="0" class="active"></li>
                                            <li data-target="#carouselExampleControlsHindiMobile" data-slide-to="1"></li>
                                            <li data-target="#carouselExampleControlsHindiMobile" data-slide-to="2"></li>
                                            <li data-target="#carouselExampleControlsHindiMobile" data-slide-to="3"></li>
                                        </ol>
                                    </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Numbers ENDS-->

        <!-- About STARTS-->
        <div class="container-fluid campaign-about-yip" id="campaign-partners">
            <div class="row d-none d-sm-flex d-md-flex d-lg-flex d-xl-flex no-gutters campaign-about-yip-inner">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 d-sm-flex d-md-flex d-lg-flex d-xl-flex align-content-center justify-content-start flex-wrap" ></div>
                
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 d-sm-flex d-md-flex d-lg-flex d-xl-flex flex-column about-yip-content">
                    <div class="about-yip-contento-inner">
                        <h1 class="align-self-end align-self-md-start align-self-lg-end align-self-xl-end"><?php echo $this->lang->line('ab_yip_heading'); ?></h1>
                        <p class="align-self-end"><?php echo $this->lang->line('ab_yip_content'); ?></p>
                        <p class="align-self-end"><?php echo $this->lang->line('ab_yip_p1'); ?></p>
                        <ul>
                            <li><?php echo $this->lang->line('ab_yip_p2'); ?></li>
                            <li><?php echo $this->lang->line('ab_yip_p3'); ?></li>
                            <li><?php echo $this->lang->line('ab_yip_p4'); ?></li>
                            <li><?php echo $this->lang->line('ab_yip_p5'); ?></li>
                        </ul>
                        <div class="about-yip-image">
                            <?php
                                if($current_lang == 'en') {
                            ?>
                                    <img src="<?php echo base_url() ?>assets/images/yip_process.png"/>
                            <?php
                                } else {
                            ?>
                                    <img src="<?php echo base_url() ?>assets/images/yip_process_hindi.png"/>
                            <?php
                                }
                            ?>
                        </div>
                        <div class="button_register_div"><button class="button_register"><?php echo $this->lang->line('register_now'); ?></button></div>
                    </div>
                </div>
            </div>
            <div class="row d-block d-sm-none">
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12  col-xs-12 about-yip-contento-inner-mobile">
                    <h1 class="align-self-end align-self-md-start align-self-lg-end align-self-xl-end"><?php echo $this->lang->line('ab_yip_heading'); ?></h1>
                    <p class="align-self-end"><?php echo $this->lang->line('ab_yip_content'); ?></p>
                    <p class="align-self-end"><?php echo $this->lang->line('ab_yip_p1'); ?></p>
                    <ul>
                        <li><?php echo $this->lang->line('ab_yip_p2'); ?></li>
                        <li><?php echo $this->lang->line('ab_yip_p3'); ?></li>
                        <li><?php echo $this->lang->line('ab_yip_p4'); ?></li>
                        <li><?php echo $this->lang->line('ab_yip_p5'); ?></li>
                    </ul>
                    <div class="about-yip-image">
                        <?php
                            if($current_lang == 'en') {
                        ?>
                                <img src="<?php echo base_url() ?>assets/images/yip_process.png"/>
                        <?php
                            } else {
                        ?>
                                <img src="<?php echo base_url() ?>assets/images/yip_process_hindi.png"/>
                        <?php
                            }
                        ?>
                    </div>
                    <div class="button_register_div"><button class="button_register"><?php echo $this->lang->line('register_now'); ?></button></div>
                </div>
            </div>
        </div>
        <!-- About ENDS-->

        <!-- Upcoming Events Start -->
        <div class="container-fluid campaign-events-yip">
            <div class="container">
                <h1 class="align-self-end align-self-md-start align-self-lg-end align-self-xl-end"><?php echo $this->lang->line('up_yip_events'); ?></h1>
                <div class="card-deck">
                    <div class="row justify-content-start">
                        <div class="col-12 col-md-8">
                            <div class="card mb-4 card_events mx-auto">
                                <img class="card-img-top img-fluid" src="<?php echo base_url(); ?>/assets/images/events_1.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <div class="event_details_name">
                                        <p class="date"><span>8</span><br/>Feb</p>
                                        <p class="card-text" title="Understanding the indian">Understanding the indian political problems</p>
                                        <p class="text-muted" title="Gachibowli Stadium - Hyderabad">Gachibowli Stadium - Hyderabad</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="w-100 d-none d-sm-block d-md-none"><!-- wrap every 2 on sm--></div>
                        <div class="col-12 col-md-4">
                            <div class="card mb-4 mx-auto">
                                <h2 class="yip_event_list"><i class="fa fa-star" aria-hidden="true"></i> <?php echo $this->lang->line('yip_list'); ?></h2>
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0"><button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">CFA & I-PAC Society India Thought Leadership Series</button></h5>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                <p><strong><?php echo $this->lang->line('where'); ?>:</strong> ISB, Hyderabad</p>
                                                <p><strong><?php echo $this->lang->line('when'); ?>:</strong> 26 Dec, Saturday, 4:00 PM Onwards</p>
                                                <p><strong><?php echo $this->lang->line('topic'); ?>:</strong> Youth In Politics</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0"><button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Laboris nisi ut aliquip ex ea commodo consequat.</button></h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                                <p><strong><?php echo $this->lang->line('where'); ?>:</strong> ISB, Hyderabad</p>
                                                <p><strong><?php echo $this->lang->line('when'); ?>:</strong> 26 Dec, Saturday, 4:00 PM Onwards</p>
                                                <p><strong><?php echo $this->lang->line('topic'); ?>:</strong> Youth In Politics</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingThree">
                                            <h5 class="mb-0"><button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Cillum dolore eu fugiat nulla pariatur. Excepteur</button></h5>
                                        </div>
                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                            <div class="card-body">
                                                <p><strong><?php echo $this->lang->line('where'); ?>:</strong> ISB, Hyderabad</p>
                                                <p><strong><?php echo $this->lang->line('when'); ?>:</strong> 26 Dec, Saturday, 4:00 PM Onwards</p>
                                                <p><strong><?php echo $this->lang->line('topic'); ?>:</strong> Youth In Politics</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingFour">
                                            <h5 class="mb-0"><button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Sunt in culpa qui officia deserunt mollit anim.</button></h5>
                                        </div>
                                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                            <div class="card-body">
                                                <p><strong><?php echo $this->lang->line('where'); ?>:</strong> ISB, Hyderabad</p>
                                                <p><strong><?php echo $this->lang->line('when'); ?>:</strong> 26 Dec, Saturday, 4:00 PM Onwards</p>
                                                <p><strong><?php echo $this->lang->line('topic'); ?>:</strong> Youth In Politics</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Upcoming Events Ends -->

        <!--  -->
        <div class="container-fluid about-prashant">
            <div class="container">
                <div class="row justify-content-start">
                    <div class="col-12 col-md-6 col-sm-12 col-xs-12" >
                        <h1 class="align-self-start align-self-md-start align-self-lg-start align-self-xl-start mobile"> 
                            <?php echo $this->lang->line('ab_pk'); ?>
                        </h1>
                        <div id='pk_1' class="desktop">
                            <img src="<?php echo base_url();?>assets/images/video-background.png" />
                        </div>
                        <div id='pk_2'>
                            <img src="<?php echo base_url();?>assets/images/about_pk_video.jpg" />
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-sm-12 col-xs-12">
                        <h1 class="align-self-start align-self-md-start align-self-lg-start align-self-xl-start desktop"> 
                            <?php echo $this->lang->line('ab_pk'); ?>
                        </h1>
                        <p class="align-self-end align-self-start align-self-md-start align-self-lg-start align-self-xl-start ">
                            <?php echo $this->lang->line('ab_pk_content'); ?>
                        </p>
                        <a href="<?php echo base_url();?>home/organizations" class="align-self-start  align-self-md-start align-self-lg-start align-self-xl-start">
                            <img src="<?php echo base_url();?>assets/images/twitter-handle.png" height="45px"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- About IPAC Start -->
        <div class="container-fluid about-ipac">
            <div class="container">
                <div class="row justify-content-start">
                    <div class="col-12 col-md-6 col-sm-12 col-xs-12">
                        <h1><?php echo $this->lang->line('ab_ipac'); ?></h1>
                        <p><?php echo $this->lang->line('ab_ipac_content'); ?></p>
                        <div class="about_ipac_buttons desktop">    
                            <div class="button_register_div"><button class="button_register"><?php echo $this->lang->line('register_now'); ?></button></div>
                            <div class="follow_images">
                                <img src="<?php echo base_url()?>assets/images/3 icons-tweet.png" height="30px">
                                <img src="<?php echo base_url()?>assets/images/3 icon fb.png" height="30px">
                                <img src="<?php echo base_url()?>assets/images/3 icons insta.png" height="30px">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12 col-xs-12">
                        <div class="video-image-overlay">
                            <div class="close_video">Close</div>
                            <img src="<?php echo base_url()?>assets/images/ab_ipac.png" data-video="https://www.youtube.com/embed/c4Zc8yc7Mr0?&autoplay=1&rel=0&fs=0&showinfo=0&autohide=3&modestbranding=1">
                            <div class="about_ipac_video"></div>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-start mobile">
                    <div class="col-12 col-md-6 col-sm-12 col-xs-12 about_ipac_buttons">    
                        <div class="button_register_div"><button class="button_register"><?php echo $this->lang->line('register_now'); ?></button></div>
                        <div class="follow_images">
                            <img src="<?php echo base_url()?>assets/images/3 icons-tweet.png" height="30px">
                            <img src="<?php echo base_url()?>assets/images/3 icon fb.png" height="30px">
                            <img src="<?php echo base_url()?>assets/images/3 icons insta.png" height="30px">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Gallery Starts -->
        <div class="container-fluid campaign-gallery-yip">
            <div class="container" >
                <h1 class="align-self-end align-self-md-start align-self-lg-end align-self-xl-end"><?php echo $this->lang->line('yip_gallery_heading'); ?></h1>
                <div id="carouselGallery" class="desktop carousel gallery_main slide" data-ride="carousel" data-interval="7000">
                    <div class="carousel-inner gallery_carosuel_inner row w-100 mx-auto" role="listbox">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card last_slide rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                            </div><!--.row-->
                        </div><!--.item-->
                        <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3 gallery_card">
                                    <div class="inner_gallery_carosuel rounded-top" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                                <div class="col-md-3 gallery_card last_slide rounded-top">
                                    <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                        <img class="img-fluid mx-auto d-block rounded-top" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                        <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                        <h2>Hyderabad Gallery</h2>
                                    </div>
                                </div>
                            </div><!--.row-->
                        </div><!--.item-->
                    </div>
                    <a class="carousel-control-prev" href="#carouselGallery" role="button" data-slide="prev">
                        <img src="<?php echo base_url();?>assets/images/left_arrow.png" alt="Prev">
                    </a>
                    <a class="carousel-control-next text-faded" href="#carouselGallery" role="button" data-slide="next">
                        <img src="<?php echo base_url();?>assets/images/right_arrow.png" alt="Next">
                    </a>
                </div>

                <div id="carouselGalleryMobile" class="carousel slide mobile">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                <img class="img-fluid mx-auto d-block" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                <h2>Hyderabad Gallery</h2>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                <img class="img-fluid mx-auto d-block" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                <h2>Hyderabad Gallery</h2>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                <img class="img-fluid mx-auto d-block" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                <h2>Hyderabad Gallery</h2>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="inner_gallery_carosuel" data-toggle="modal" href="#myModal">
                                <img class="img-fluid mx-auto d-block" src="<?php echo base_url(); ?>assets/images/yip-gallery-1.png" alt="slide 1">
                                <p class="location_marker"><i class="fa fa-map-marker" aria-hidden="true"></i> Location</p>
                                <h2>Hyderabad Gallery</h2>
                            </div>
                        </div>
                    </div>
                    <ol class="carousel-indicators">
                        <li data-target="#carouselGalleryMobile" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselGalleryMobile" data-slide-to="1"></li>
                        <li data-target="#carouselGalleryMobile" data-slide-to="2"></li>
                        <li data-target="#carouselGalleryMobile" data-slide-to="3"></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- Gallery Ends -->