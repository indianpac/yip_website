<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['enter_politics'] = 'Do you want to enter politics?';
$lang['enter_politics_description'] = 'Come join Youth in Politics to start your political journey under the mentorship of Prashant Kishor';
$lang['register_now'] = 'Register Now';
$lang['see_how_it_works'] = 'See how it works';

$lang['total_registerations']="Total Registerations";
$lang['tot_events']="Total Events";
$lang['tot_dis']="Total Districts";
$lang['tot_att']="Event Attendees";

$lang['fact_heading']="India is Young but our politicians are Old";
$lang['ab_yip_heading']="About Youth in Politics";
$lang['ab_yip_content']="Youth in Politics (YIP) is a first-of-its kind pan-India platform for youth between 18 & 35 years of age, to join active politics under the mentorship of Prashant Kishor & I-PAC. Set up with a vision to transform the political landscape into one where youth find a proportionate voice, YIP supports and mentors young leaders as they take the plunge into active electoral politics.";
$lang['ab_yip_p1']="Whether you are a professional looking to start from your workplace, a college student stepping into student politics or a youth in a village aspiring to become a Sarpanch, YIP offers an opportunity for you to-";
$lang['ab_yip_p2']="Leverage the political acumen of Prashant Kishor";
$lang['ab_yip_p3']="Receive professional guidance from I-PAC";
$lang['ab_yip_p4']="Receive tailor made mentorship plans";
$lang['ab_yip_p5']="Connect with like-minded aspirants across India";

$lang['up_yip_events']="Upcoming YIP Events";
$lang['yip_list']="YIP Event List";

$lang['where']="Where:";
$lang['when']="When:";
$lang['topic']="Topic:";

$lang['ab_pk']="About Prashant Kishor";
$lang['ab_pk_content']="Recognized as India’s most sought after political strategist, Prashant Kishor has revolutionized election campaigning in India by providing a platform for young professionals to meaningfully contribute to electoral politics. Mentoring both CAG and I-PAC, he has helped many top leaders including Narendra Modi, Nitish Kumar, and Captain Amarinder Singh attain top public offices.<br/><br/> Since 2018, Prashant has joined active politics and has also been appointed as the National Vice President of JD(U). With a string of wins and with successful mentorship of young professionals (first CAG, then I-PAC) to his name, Prashant is now committed to opening up politics for young professionals and leaders.";

$lang['ab_ipac']="About I-PAC";
$lang['ab_ipac_content']="Indian Political Action Committee (I-PAC) is the platform of choice for students and young professionals to participate in and make meaningful contribution to political affairs and governance of the country, without necessarily being part of a political party. Since 2013, I-PAC has brought some of the best minds from diverse academic and professional backgrounds together. I-PAC works with visionary leaders, helps them set a citizen-centric agenda and partners with them to conceptualize & implement effective methods of taking it to the masses.";

$lang['yip_gallery_heading']="Youth in Politics Gallery";


$lang['concept_note']="YIP Concept Note";
$lang['faq']="YIP FAQs";
$lang['privacy_policy']="Privacy Policy";
$lang['terms']="Terms & Conditions";
$lang['link_faq']="https://www.youthinpolitics.in/faq/faq_en.html";