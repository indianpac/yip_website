<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = 'findcontent';
$route['translate_uri_dashes'] = TRUE;
$route['^(\w{2})$'] = $route['default_controller'];